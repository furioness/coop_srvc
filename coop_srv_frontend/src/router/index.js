import Vue from 'vue'
import VueRouter from 'vue-router'
import Welcome from "@/views/Welcome";
import Login from "@/views/Login";
import Register from "@/views/Register";
import Logout from "@/views/Logout";
import Calendar from "@/views/Calendar";
import Projects from "@/views/Projects";
import Profile from "@/views/Profile";
import Search from "@/views/Search";
import Main from "@/views/Main";
import store from "@/store"


Vue.use(VueRouter)

const routes = [
    {
        path: '/welcome', component: Welcome,
        children: [
            {path: '', redirect: {name: 'login'}},
            {path: 'login', name: 'login', component: Login},
            {path: 'register', name: 'register', component: Register}]
    },
    {
        path: '', component: Main, meta: {requiresAuth: true}, redirect: {name: 'profile', params: {userId: 'me'}},
        children: [
            {
                path: '/profile/:userId',
                name: 'profile',
                component: Profile,
                props: route => ({id: route.params.userId || 'me'})
            },
            {path: '/logout', name: 'logout', component: Logout},
            {path: '/calendar', name: 'calendar', component: Calendar},
            {path: '/search', name: 'search', component: Search},
            {path: '/projects', name: 'projects', component: Projects},
        ]
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth) && !store.getters.loggedIn) {
        next({name: 'login'})
    }
    next()
})

export default router
