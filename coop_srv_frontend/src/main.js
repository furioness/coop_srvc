import Vue from 'vue'
import VueModal from '@kouts/vue-modal';
import VeeValidate from 'vee-validate'
import '@kouts/vue-modal/dist/vue-modal.css';
import {BootstrapVue, BootstrapVueIcons} from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Chat from 'vue-beautiful-chat'


import App from './App.vue'
import router from './router'
import store from './store'
import TouchDatePicker from "@/components/TouchDatePicker";


Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(Chat)
Vue.use(VeeValidate);


Vue.component('Modal', VueModal);
Vue.component('Datepicker', TouchDatePicker)

Vue.config.productionTip = false

new Vue({
    router,
    store,
    components: {},
    render: h => h(App)
}).$mount('#app')
