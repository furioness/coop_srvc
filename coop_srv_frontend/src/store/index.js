import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VuexPersistence from 'vuex-persist'
import {createStore} from 'vuex-extensions'
import apiService from '@/service'
import * as socketService from '../service/socket'

import router from '../router'

const vuexPersist = new VuexPersistence({
    strictMode: true,
    storage: window.localStorage
})

Vue.use(Vuex)


export default createStore(Vuex.Store, {
    strict: true,
    plugins: [vuexPersist.plugin],
    state: {
        users: {},
        jwt: null
    },
    mutations: {
        SET_USER_DATA(state, {userId, userData}) {
            Vue.set(state.users, userId, userData)
            //state.users[userId] = userData
        },
        SET_JWT(state, accessToken) {
            state.jwt = accessToken
        },
        RESET_STATE() {
            this.reset()
        },
        RESTORE_MUTATION: vuexPersist.RESTORE_MUTATION
    },
    actions: {
        register({commit}, credentials) {
            return axios
                .post('//localhost:8000/auth/register', credentials)
                .then(({data}) => {
                    commit('SET_USER_DATA', data)
                })
        },
        login({commit, dispatch}, credentials) {
            return apiService.login(credentials)
                .then(({data}) => {
                    commit('SET_JWT', data.access_token)
                    dispatch('authenticateChat')
                })
        },
        authenticateChat({state}) {
            socketService.authenticate(state.jwt)
        },
        logout({commit}) {
            commit('RESET_STATE')
            socketService.logout()
            router.replace({name: 'login'})
            //location.reload()
        },
        loadProfile({state, commit}, {userId = 'me', update = false}) {
            console.log('in action loadProfile', update, state.users[userId])
            if (update || !state.users[userId])
                return apiService.loadProfile(userId)
                    .then(({data}) => {
                        console.log('in action promise')
                        commit('SET_USER_DATA', {userId, userData: data})
                    })
            return Promise.resolve()
        },
        loadSectionsProjects({state, commit, dispatch}, userId = 'me') {
            return dispatch('loadProfile', userId)
        },
        updateProfile({state, dispatch}, userData) {
            return apiService.updateProfile(userData)
                .then(() => dispatch('loadProfile', {update: true}))
        },
        addSection({commit, state, dispatch}, sectionData) {
            let updatedUser = Object.assign({}, state.users.me)
            console.log(state.users.me, updatedUser, sectionData)
            updatedUser.sections.push(sectionData)

            return dispatch('updateProfile', updatedUser)
        }
    },
    getters: {
        loggedIn(state) {
            return !!state.jwt
        },
        jwt(state) {
            return state.jwt
        },
        myProfile(state) {
            if (!state.users?.me)
                throw "Accessing user's own profile before its initializing"
            return state.users.me
        }

    }
})
