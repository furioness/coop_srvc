import {io as sio} from "socket.io-client";
import store from "@/store";
import {SERVER_URL} from '@/config'


let socket = null

function init() {
    socket = sio(SERVER_URL, {
        path: '/chat/socket.io',
        transports: ['websocket']
    })
    window.addEventListener("onbeforeunload", () => logout())
}

function authenticate(jwt) {
    init()
    socket.emit('authenticate', jwt)
    return new Promise((resolve, reject) => {
        socket.on('authentication', (resp) => {
            console.log('authentication', resp)
            if (resp == "success")
                resolve(resp)
            reject(resp)
        })
    })

}

function subscribeForMessages(callback) {
    socket.on()
}

function logout() {
    socket.close("logout")
    socket = null
}

function retrieveMessages() {
    return new Promise((resolve) => {
        console.log('retrieve_messages emitted')
        socket.emit('retrieveMessages')
        socket.on('oldMessages', resp => {
            console.log('resp!!!', resp.messages);
            resolve(resp.messages)
        })
    })
}

function sendMessage(message) {
    return new Promise((resolve) => {
        console.log('in socket sendMessage', message)
        socket.emit('sendMessage', message)
        socket.on('newMessageAccepted', resp => resolve(resp))
    })
}



export {socket, authenticate, logout, retrieveMessages, sendMessage}
