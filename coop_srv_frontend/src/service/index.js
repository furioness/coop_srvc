import axios from 'axios'
import store from '../store'
import {SERVER_URL} from '@/config'


console.log(SERVER_URL)

const apiClient = axios.create({
    baseURL: SERVER_URL,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    }
})


apiClient.interceptors.request.use(function (config) {
    config.headers.Authorization = "Bearer " + store.getters.jwt;
    return config;
});


export default {
    login(credentials) {
        let form_data = new FormData();

        for (let key in credentials) {
            form_data.append(key, credentials[key]);
        }

        return apiClient.post('//localhost:8000/auth/jwt/login', form_data, {
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
    },
    loadProfile(userId = 'me') {
        console.log('in loadprifle service')
        if (userId == 'me')
            return apiClient.get(`/users/${userId}`)
        return apiClient.get(`/users/${userId}/public`)
    },
    updateProfile(userData) {
        return Promise.all([
            apiClient.patch('/users/me', userData)
            //,
            // apiClient.post('/users/me/avatar',
            //     userData.avatar, {headers: {'Content-Type': 'multipart/form-data'}}
            // )
        ])
    },
    updateSections(updatedSectionList) {
        return apiClient.patch('/users/me', {sections: updatedSectionList})
    }
}
