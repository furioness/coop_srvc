import uvicorn

from app import app


if __name__ == "__main__":
    import asyncio
    import uvicorn
    loop = asyncio.get_event_loop()
    config = uvicorn.Config(app=app, port=8000, loop=loop, access_log=True)
    server = uvicorn.Server(config)
    loop.run_until_complete(server.serve())
