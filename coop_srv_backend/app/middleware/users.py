from fastapi_users import FastAPIUsers
from app import database as db
from app.models import user as user_models

from fastapi_users.authentication import JWTAuthentication

from app.config import SECRET

auth_backends = []

jwt_authentication = JWTAuthentication(secret=SECRET, lifetime_seconds=3600)

auth_backends.append(jwt_authentication)

fastapi_users = FastAPIUsers(
    db.user_db,
    auth_backends,
    user_models.User,
    user_models.UserCreate,
    user_models.UserUpdate,
    user_models.UserDB,
)
