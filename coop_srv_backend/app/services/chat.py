# from redis

from app.database import chat_coll
from app.models.chat import ChatMessages, ChatRoom, ChatUser, ChatMessage


async def get_messages():
    return ChatMessages(messages=(await chat_coll.find_one({}))['messages'])


async def init_main_chat():
    if await chat_coll.count_documents({}) == 0:
        await chat_coll.insert_one(ChatRoom().dict())


async def append_user(user):
    chat: ChatRoom = await chat_coll.find_one({})
    await chat.users.append(ChatUser(user))


async def append_message(user, message):
    chat: ChatRoom = await chat_coll.find_one({})
    message = ChatMessage(sender_id=user.id, message=message)

    await chat_coll.update_one(chat, {"$push": {"messages": message.dict()}})
    return message.id
