import os

DEFAULT_AVATAR_PATH = '/user.png'
DATABASE_URL = os.environ.get("MONGODB_URL", "mongodb://localhost:27017")
SECRET = "SECRET"
CORS_ACCEPT_ORIGIN = '*'
