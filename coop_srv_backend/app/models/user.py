from pathlib import Path
from typing import List, Optional
from datetime import datetime


from fastapi_users import models
from pydantic import BaseModel, UUID4, validator

from app.config import DEFAULT_AVATAR_PATH
from app.models.misc import Language
from app.models.notifications import Notification
from app.models.sections import Section



class User(models.BaseUser):
    is_active: bool = True
    first_name: str
    last_name: str
    birth_date: Optional[datetime]
    languages: Optional[Language]
    country: Optional[str]
    about: Optional[str]
    avatar: str = DEFAULT_AVATAR_PATH
    notifications: Optional[List[Notification]] = []
    sections: Optional[List[Section]] = []


class UserPublic(BaseModel):
    id: UUID4
    first_name: str
    last_name: str
    birth_date: Optional[datetime]
    languages: Optional[Language]
    country: Optional[str]
    about: Optional[str]
    avatar: str = DEFAULT_AVATAR_PATH


class UserCreate(models.BaseUserCreate):
    first_name: str
    last_name: str

    @validator('password')
    def valid_password(cls, v: str):
        if len(v) < 6:
            raise ValueError('Password should be at least 6 characters')
        return v


class UserUpdate(User, models.BaseUserUpdate):
    pass


class UserDB(User, models.BaseUserDB):
    pass
