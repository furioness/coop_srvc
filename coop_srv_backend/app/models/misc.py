from datetime import datetime

from pydantic import BaseModel
from bson.objectid import ObjectId as BsonObjectId


class DailyStats(BaseModel):
    date: datetime
    score: float
    target_score: float


class Language(BaseModel):
    lang_name: str
    exp_level: int

class PydanticObjectId(BsonObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not isinstance(v, BsonObjectId):
            print(v)
            raise TypeError('ObjectId required')
        return str(v)

    def __str__(self):
        return str(self)

    @classmethod
    def __modify_schema__(cls, field_schema):
        print(field_schema)
        # __modify_schema__ should mutate the dict it receives in place,
        # the returned value will be ignored
        field_schema.update(
            format="uuid4",
            type="string",
            description="BsonObjectId generated by MongoDB"
        )
