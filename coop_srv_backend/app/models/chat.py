from datetime import datetime
from typing import List
from uuid import uuid4

from pydantic import BaseModel, Field, UUID4


class Message(BaseModel):
    type: str
    data: str


class ChatMessage(BaseModel):
    id: UUID4 = Field(default_factory=uuid4)
    sender_id: UUID4
    message: Message
    time: datetime = Field(default_factory=datetime.now)


class ChatMessages(BaseModel):
    messages: List[ChatMessage]

class ChatUser(BaseModel):
    id: UUID4
    first_name: str


class ChatRoom(BaseModel):
    id: UUID4 = Field(default_factory=uuid4)
    name: str = "Default room"
    messages: List[ChatMessage] = []
    users: List[ChatUser] = []
