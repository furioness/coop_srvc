from datetime import datetime
from typing import List, Optional

from pydantic import BaseModel

from app.models.misc import DailyStats, PydanticObjectId
from app.models.tasks import Task


class Project(BaseModel):
    id: PydanticObjectId
    name: str
    tags: Optional[List[str]] = []
    description: str
    is_public: bool
    start_date: datetime
    end_date: datetime
    chat_room_ids: Optional[List[PydanticObjectId]] = []
    stats: Optional[List[DailyStats]] = []
    tasks: Optional[List[Task]] = []
