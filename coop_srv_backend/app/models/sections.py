from typing import List, Optional

from pydantic import BaseModel, Field, UUID4
from bson.objectid import ObjectId
from uuid import UUID, uuid4

from app.models.misc import DailyStats, PydanticObjectId
from app.models.projects import Project


class Section(BaseModel):
    id: UUID4 = Field(default_factory=uuid4)
    name: str
    tags: Optional[List[str]] = []
    description: str
    stats: Optional[List[DailyStats]] = []
    projects: Optional[List[Project]] = []
