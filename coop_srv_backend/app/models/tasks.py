from datetime import datetime
from typing import List, Optional, Type

from pydantic import BaseModel
from bson.objectid import ObjectId

from app.models.misc import PydanticObjectId


class TaskInputTerm(BaseModel):
    type: str
    target_value: float
    label: str


class Checkin(BaseModel):
    date: datetime
    input_term: TaskInputTerm
    value: float
    target_value: float



class Task(BaseModel):
    id: PydanticObjectId
    name: str
    tags: Optional[List[str]] = []
    description: str
    start_date: datetime
    end_date: datetime
    input_term: TaskInputTerm
    repeating: str
    checkins: Optional[List[Checkin]] = []



