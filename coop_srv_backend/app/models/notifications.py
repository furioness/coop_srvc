from datetime import datetime

from pydantic import BaseModel
from app.models.misc import PydanticObjectId


class Notification(BaseModel):
    id: PydanticObjectId
    sender_id: PydanticObjectId
    text: str
    timestamp: datetime
    was_read: bool
