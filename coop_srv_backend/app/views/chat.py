import inspect
import ujson

import socketio

from app import CORS_ACCEPT_ORIGIN
from app.database import user_db
from app.middleware.users import jwt_authentication
from app.services import chat as chat_srvc


sio = socketio.AsyncServer(async_mode="asgi", cors_allowed_origins=CORS_ACCEPT_ORIGIN)
sio_app = socketio.ASGIApp(socketio_server=sio, socketio_path='socket.io')

USERS = {}


def check_auth(func):
    async def inner(sid, *args, **kwargs):
        if sid not in USERS:
            print(f'check auth failure for sid {sid}')
            await sio.emit('authentication', 'error')
            await sio.disconnect('Unknown sid')
            return

        return await func(sid, *args, **kwargs)

    return inner


@sio.on('sendMessage')
@check_auth
async def send_message(sid, message):
    print('in sendMessage', sid, message)
    msg_id = await chat_srvc.append_message(USERS[sid], message)
    await sio.emit('newMessageAccepted', {'msgId': str(msg_id)})


@sio.on('connect')
def connected(sid, env):
    print(f'{sid} connected, in USERS is {sid in USERS}')


@sio.on('disconnect')
def disconnect(sid, *args):
    try:
        del USERS[sid]
    except KeyError:
        pass

    print(sid, 'disconnected')


@sio.on('authenticate')
async def authenticate(sid, jwt: str):
    user = await jwt_authentication(jwt, user_db)

    if not user:
        print('authentication error - no user', sid)
        return await sio.emit('authentication', 'error')

    USERS[sid] = user
    print(f'User {user.id} {user.first_name} authenticated with sid {sid}')
    await sio.emit('authentication', 'success')


@sio.on('retrieveMessages')
@check_auth
async def retrieve_messages(sid):
    print('retrieve_messages emitted')
    msgs = await chat_srvc.get_messages()
    print(msgs)
    await sio.emit('oldMessages', ujson.loads(msgs.json()))
    print('old_messages emitted')
