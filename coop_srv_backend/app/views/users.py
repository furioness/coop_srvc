from fastapi import APIRouter, Depends, HTTPException, status
from pydantic import UUID4

from app.middleware.users import fastapi_users
from app.models.user import UserPublic
from app.database import user_db

users_router = APIRouter()


@users_router.get('/{user_id}/public', response_model=UserPublic,
                  dependencies=[Depends(fastapi_users.get_current_user)])
async def get_user(user_id: UUID4):
    user = await user_db.get(user_id)
    if user is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    return user

