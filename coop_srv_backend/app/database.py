from motor import motor_asyncio
from motor.core import AgnosticCollection
from fastapi_users.db import MongoDBUserDatabase
import aioredis

from app.models.user import UserDB
from app.config import DATABASE_URL

# redis_conn = aioredis.Redis(host = '127.0.0.1', port = 6379)
client = motor_asyncio.AsyncIOMotorClient(DATABASE_URL, uuidRepresentation="standard")

# redis_conn.publish('test')

db = client["accountability"]

user_coll: AgnosticCollection = db["Users"]
chat_coll: AgnosticCollection = db["ChatRooms"]
user_db = MongoDBUserDatabase(UserDB, user_coll)


