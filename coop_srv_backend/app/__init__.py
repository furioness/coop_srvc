from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles

from app.config import CORS_ACCEPT_ORIGIN
from app.middleware.users import fastapi_users, jwt_authentication
from app.views.users import users_router
from app.views.chat import sio_app
from app.services.chat import init_main_chat

app = FastAPI()

app.mount('/chat', sio_app)

@app.on_event("startup")
async def startup_event():
    await init_main_chat()


app.add_middleware(
    CORSMiddleware,
    allow_origins=CORS_ACCEPT_ORIGIN,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(
    fastapi_users.get_auth_router(jwt_authentication),
    prefix="/auth/jwt",
    tags=["auth"],
)

app.include_router(
    fastapi_users.get_register_router(),
    prefix="/auth",
    tags=["auth"],
)

app.include_router(
    fastapi_users.get_users_router(),
    prefix="/users",
    tags=["users"],
)

app.include_router(users_router,
                   prefix="/users",
                   tags=["users"])

app.mount("/", StaticFiles(directory="static", html=True), name="static")
